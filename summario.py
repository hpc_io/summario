#!/usr/bin/env python3

import bz2
import pickle
import sys
import subprocess
from trace import Trace
from output_file import OutputFile
from usage import get_parameters



#check and obtain parameters
(darshan,threshold,iterative,verbose,simple) = get_parameters()
#find all files in the provided folder
if (".bz2.parsed" in sys.argv[1]) or (".darshan.parsed" in sys.argv[1]):
    ls = [""] #the name of the dir is actually a single filename
    name_parts = sys.argv[1].split('.')
    for i in range(len(name_parts)):
        if name_parts[i] == "darshan":
            break
        if name_parts[i] == "bz2":
            break
    assert i < len(name_parts)
    name_parts = name_parts[:i]
    prefix = ""
    for elem in name_parts:
        if prefix != "":
            prefix += "."
        prefix += elem
    prefix += "_"
else:
    ls = subprocess.getoutput("ls "+sys.argv[1]).split("\n")
    if sys.argv[1][len(sys.argv[1])-1] != '/':
        sys.argv[1] += "/"
    prefix = sys.argv[1]
#prepare the output files
if not simple:
    jobfile = OutputFile(prefix+"jobs.csv", header="job;app;user;start;end;nprocs;nodes;bytes;io_time;unique_files;shared_files")
    phasefile = OutputFile(prefix+"phases.csv", header = "job;start;end;type;rank;bytes;time;api;reqs;spatiality;files;filenb;time_var;bytes_var;subset_mode;subset_avg;subset_var")
#read and estimate phases from them
for filename in ls:
    #if the argument was a single file name, ls will contain one
    #element (""). When we pass sys.argv[1]+filename to the Trace
    #constructor, we'll be giving that filename (because it is in
    #sys.argv[1] and filename will be "")
    if (filename != "") and (not ".parsed" in filename):
        continue
#    print(sys.argv[1]+filename)
    trace = Trace(sys.argv[1]+filename, threshold, iterative, verbose, darshan, simple)
    if simple:
        simple_trace = trace.get_simple_trace()
        if simple_trace == None:
            continue #nothing to see in this file
        data = pickle.dumps(simple_trace)
        assert filename != ""
        with bz2.open(filename.split('.')[0]+".pkl.bz2", "wb") as fd:
            fd.write(bz2.compress(data))
    if not simple:
        jobfile.write(trace.get_job_info())
        phasefile.write(trace.get_phases_info())
#finish writing and close the output files
if not simple:
    jobfile.close()
    phasefile.close()
#else:
#    with open("all_data.pkl", "wb") as fd:
#        pickle.dump(all_data, fd)
