from datetime import datetime

class JobInformation:
    """
    job-level information about a job.

    Attributes
    ----------
    info : dictionary<str, *>
        A dictionary containing all the information about the
        job. Possible keys are : app, user, start, end, nprocs,
        nodes, bytes, io_time, unique_files, shared_file
    darshan : the darshan version used to generate and parse the
        traces

    Methods
    -------
    decode_line(List<str>, str) :
        to fill the object with information coming from a line
        of the trace file.
    remove_metadata_files(int) :
        eliminate a number (given as parameters) of files from
        the unique_files counter
    print() :
        return a string representation of this object
    get(str) :
        return the value associated with the key given as
        parameter
    """
    def __init__(self, darshan, parse_in_2024=False, filename="", jobid=""):
        self.info = {}
        self.darshan = darshan
        self.parse_in_2024 = parse_in_2024
        self.filename = filename
        self.job_id = jobid

    def decode_line(self, parsed_line, line):
        """
        Takes a line of the trace file and decodes the
        information about the current job.

        Parameters
        ----------
        parsed_line : List<str>
            A line of the trace file broken into a list with the
            split() function
        line : str
            The same line of the trace file

        Returns
        -------
        boolean
            True if that was the last line about the job-
            level information (i.e. if we are about to
            start reading the table of file accesses. False
            otherwise.
        """
        if "# exe:" in line:    #header, name of the executable
            assert not ("app" in self.info)
            self.info["app"] = parsed_line[2]
        elif "# uid:" in line:    #header, user identifier
            assert not ("user" in self.info)
            self.info["user"] = parsed_line[2]
        elif "# start_time:" in line:    #header, job start time
            assert not ("start" in self.info)
            self.info["start"] = int(parsed_line[2])
        elif "# end_time:" in line:     #header, job end time
            assert not ("end" in self.info)
            self.info["end"] = int(parsed_line[2])
        elif "# nprocs:" in line:    #header, number of processes
            assert not ("nprocs" in self.info)
            self.info["nprocs"] = int(parsed_line[2])
        elif (self.darshan == 2) and ("# metadata: cn =" in line):    #header, number of nodes (Intrepid data set)
            assert not ("nodes" in self.info)
            self.info["nodes"] = int(parsed_line[4])
        elif (self.darshan == 2) and ("# total_bytes:" in line):    #total number of bytes accessed by the application in Darshan version 2
            assert not ("bytes" in self.info)
            self.info["bytes"] = int(parsed_line[2])
        elif (self.darshan == 3) and (("total_POSIX_BYTES_READ:" in line) or ("total_MPIIO_BYTES_READ:" in line)): #total number of bytes read by the application in Darshan version 3
            if  not "bytes_read" in self.info:
                self.info["bytes_read"] = 0
            self.info["bytes_read"] += int(parsed_line[1])
        elif (self.darshan == 3) and (("total_POSIX_BYTES_WRITTEN:" in line) or ("total_MPIIO_BYTES_WRITTEN:" in line)): #total number of bytes written by the application in Darshan version 3
            if not "bytes_written" in self.info:
                self.info["bytes_written"] = 0
            self.info["bytes_written"] += int(parsed_line[1])
        elif (self.darshan == 2) and ("# agg_perf_by_slowest:" in line):    #performance, i/o time of the slowest rank regarding i/o (estimate for the job's i/o  time)
            assert not ("io_time" in self.info)
            self.info["io_time"] = float(parsed_line[2])
        elif "# unique:" in line:    #files, number of unique files that were accessed
            if not "unique_files" in self.info:
                self.info["unique_files"] = 0
            self.info["unique_files"] += int(parsed_line[2])
        elif "# shared:" in line:    #files, number of shared files that were accessed
            if not "shared_files" in self.info:
                self.info["shared_files"] = 0
            self.info["shared_files"] += int(parsed_line[2])
        elif (self.darshan == 3) and (("# description of POSIX counters:" in line) or ("# description of MPIIO counters:" in line)):  #we are about to start reading a table of counters!
            return True
        elif (self.darshan == 2) and (("#<rank>" in line) and ("<file>" in line) and ("<value>" in line)): #we are about to start reading a table of counters!
            return True

        return False

    def remove_metadata_files(self, nb_meta):
        """
        After reading the table of file access counters, we
        might have ignored some of the files because they only
        received metadata operations. In that case, discount
        them from the number of unique files.

        Parameters
        ----------
        nb_meta : int
            number of metadata-only files that were
            identified
        """
        assert "unique_files" in self.info
    #    assert nb_meta <= self.info["unique_files"]
        self.info["unique_files"] -= nb_meta

    def __str__(self):
        return str(self.info)

    def get_info(self):
        ret = self.info["app"]+";"+self.info["user"]+";"
        ret += str(self.info["start"])+";"
        ret += str(self.info["end"])+";"
        ret += str(self.info["nprocs"])+";"
        ret += str(self.info["nodes"])+";"
        ret += str(self.info["bytes"])+";"
        ret += str(self.info["io_time"])+";"
        ret += str(self.info["unique_files"])+";"
        ret += str(self.info["shared_files"])
        return ret

    def get_info_as_dict(self):
        assert self.parse_in_2024
        if len(self.info) == 0:
            return None
        ret = {
            "file": self.filename.split('/')[-1],
            "uid": self.info["user"],
            "job_id": self.job_id,
            "start_ts": datetime.fromtimestamp(self.info["start"]),
            "end_ts": datetime.fromtimestamp(self.info["end"]),
            "run_time": self.info["end"] - self.info["start"],
            "exe": self.info["app"],
            "nnodes": self.info["nodes"],
            "nprocs": self.info["procs"]
        }
        return ret
