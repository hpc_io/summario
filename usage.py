from pathlib import Path
import sys


#instruct the user on how to use this program and exits
def usage():
    print("Usage: python "+sys.argv[0]+" [input] [options]")
    print("[input] is either a file or a directory containing a set of files to be processed. The Darshan files must be parsed with `darshan-parser --all` before given to this code. We expected the parsed files to be called *.bz2.parsed or *.darshan.parsed.")
    print("[options] are:")
    print("darshan=[2 or 3]\tDarshan version used to generate and parse the trace. (The default is 3).")
    print("threshold=[floating point value]\tHow many of the requests of a phase can be of other spatialities for us to consider the spatiality of the majority to the the spatiality of the whole phase. In other words, (1-threshold)*nreqs is  how much of the accesses must be sequential/consecutive/collective/etc for the whole phase to be considered that. (The default is 0.2)")
    print("iterative=[True or False]\tShould we prompt a key from the user after each phase estimation. It may be useful when debugging. (The default is False)")
    print("verbose=[True or False]\tAlso for debugging. (The default is False)")
    print("simple=[True or False]\tIf True, do not estimate anything, just make dictionaries from the Darshan counter (2024 addition for the paper). (The default is False)")
    print("help\tPrint this message and end.")
    exit(0)

def error(msg):
    print("ERROR: "+msg)
    usage()

def get_boolean(value):
    if value == "True":
        return True
    elif value == "False":
        return False
    else:
        error("boolean option should be True or False, not "+value)

#return (darshan,threshold,iterative,verbose), see usage() for details
def get_parameters():
    #check that we are using python3
    if sys.version_info[0] != 3:
        print("ERROR: this code requires python3")
        exit(-1)
    #check that we have the correct number of parameters
    if len(sys.argv) < 2:
        error("not enough arguments")
    #check the input file/dir exists
    if not Path(sys.argv[1]).exists():
        error("Input file/dir "+sys.argv[1]+" does not exist")
    #default options
    darshan=3
    threshold=0.2
    iterative=False
    verbose=False
    simple=False
    #parse the options
    options = sys.argv[2:]
    for option in options:
        if option == "help":
            usage()
        if not ("=" in option):
            error("cannot understand option "+option)
        parsed = option.split('=')
        if parsed[0] == "darshan":
            if (parsed[1] != "3") and (parsed[1] != "2"):
                error("Darshan version must be 2 or 3, not "+parsed[1])
            darshan = int(parsed[1])
        elif parsed[0] == "threshold":
            try:
                value = float(parsed[1])
            except ValueError:
                error("could not convert value "+parsed[1]+" to a float")
            if (value < 0) or (value > 1):
                error("threshold should be between 0 and 1")
            threshold = value
        elif parsed[0] == "iterative":
            iterative = get_boolean(parsed[1])
        elif parsed[0] == "verbose":
            verbose = get_boolean(parsed[1])
        elif parsed[0] == "simple":
            simple = get_boolean(parsed[1])
        else:
            error("Unknown option "+parsed[0])
    print(f"Found darshan={darshan}, threshold={threshold}, iterative={iterative}, verbose={verbose}, and simple={simple}")
    return (darshan,threshold,iterative,verbose,simple)



