SummarIO
=========

This code estimates, for a given Darshan trace file (obtained from an application execution), the temporal behavior of the application, namely its different I/O phases and the presented access patterns. 

It was developed with the goal of using such information to try to predict the performance the application would have with different numbers of I/O nodes. For details, see the research report:

- Francieli Zanon Boito. Estimation of the impact of I/O forwarding on application performance. [Research Report] RR-9366, Inria. 2020, pp.20Available at: https://hal.inria.fr/hal-02969780

# Execution

```
python3 summario.py [input] [options]
```

*input* can be a Darshan file or a folder containing multiple ones. The files must be in text format, parsed with 

```
darshan-parser --all [trace file]
```

For details about the available options, run summario.py without any arguments.

At the end, SummarIO will generate the jobs.csv and phases.csv files. If *input* was a single file, then they will actually be named after the input file. 

- *jobs.csv* contains one entry per Darshan trace file, containing aggregated counters. These counters contain multiple issues and should not be used for now.
- *phases.csv* contains one entry per identified I/O phase of all input files. 

Both files contain headers that detail their contents.
