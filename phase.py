from numpy import min,max,var,sum,mean
from numpy.random import normal
from math import sqrt
from counter_set import CounterSet

#simple function that receives "read" or "write" and returns
#"READ" or "WRITTEN"
def read_or_written(op):
    if op == "read":
        return op.upper()
    else:
        return "WRITTEN"


class Phase:
    """
    An I/O phase to a rank (or all ranks together) to a single
    file handle.

    ...

    Attributes
    ----------
    bytes : int
        total amount of data (in bytes) accessed in this phase
    type : str
        "read" or "write", the operation
    rank : int
        the ranks involved in this phase. -1 for all processes,
        -N for a subset containing N processes, M for a
        single rank (M)
    time : float
        the time spent in I/O during this phase
    time_var : float
        the variation on the time spent by rank (if rank < 0)
    bytes_var : float
        the variation on the number of bytes per process (if
        rank < 0)
    api : str
        the api used for the access, "posix" or "mpiio"
    start, end : float
        timestamps for the beginning and the end of the phase
        (relative to the beginning of the job)
    reqs : int
        number of requests issued in this phase
    spatiality : str
        spatial access pattern during this phase. It may be:
        contiguous, sequential, random, or unknown.
    filenb : int
        number of files accessed in this phase
    files : str
        file strategy: "unique" or "shared"
    subset_avg : float
        if the phase is a shared file access but it involves
        more than one file, that means processes were actually
        sharing files between groups during this phase. If that
        is the case, this attribute is the average number of
        processes accessing each file
    subset_avg : float
        similar to subset_avg, but here it is the variance
    subset_mode : int
        similar to subset_avg, but here it is the number of
        processes that was the most common (the mode).
    rank_list : List<int>
        for shared-file phases that were manually built
        (because Darshan reported it separately to each rank),
        the list of ranks involved in the access. It is useful
        when we merge this to other phases, so we can find
        better estimates for time and number of ranks.
    darshan : int
        the darshan version used in this trace (2 or 3)

    Methods
    -------
    parse_counters :
        fills the object with information estimated from the
        counters in the CounterSet object given as argument
    time_compatible(Phase):
        verify if it the phase given as argument, which MUST
        have start >= this one's start, happens close enough
        in time for their merging to be considered.
    compatible(Phase):
        verify if the phase given as an argument is compatible
        enough to this one in order to be combined in a single
        phase.
    combine(Phase):
        combine the phase given as argument with this one.
        The caller will destroy the phase given as argument
        after using this function to store combined information
        in this object.
    """
    def __init__(self, darshan):
        self.bytes = 0
        self.time = 0
        self.filenb = 1
        self.files = "unique"
        self.subset_avg = 1
        self.subset_mode = 1
        self.rank_list = []
        self.subset_var = 0
        self.darshan = darshan
        assert self.darshan == 2 or self.darshan == 3

    def __str__(self):
        ret = "{bytes: "+str(self.bytes)+", type: "+self.type
        ret += ", rank: "+str(self.rank)+", time: "+str(self.time)
        ret += ", api: "+self.api+", reqs: "+str(self.reqs)
        ret += ", spatiality: "+self.spatiality
        ret += ", start: "+str(self.start)+", end: "+str(self.end)
        ret += ", files : "+self.files+", filenb: "+str(self.filenb)
        ret += ", time_var: "+str(self.time_var)+", bytes_var: "
        ret += str(self.bytes_var)
        ret += ", subset_mode: "+str(self.subset_mode)
        ret += ", subset_avg: "+str(self.subset_avg)
        ret += ", subset_var: "+str(self.subset_var)
        ret += "}"
        return ret

    def get_bytes_from_str(self, reqsize):
        if reqsize == "0_100":
            return 100
        elif reqsize == "100_1K":
            return 1024
        elif reqsize == "1K_10K":
            return 10*1024
        elif reqsize == "10K_100K":
            return 100*1024
        elif reqsize == "100K_1M":
            return 1024*1024
        elif reqsize == "1M_4M":
            return 4*1024*1024
        elif reqsize == "4M_10M":
            return 10*1024*1024
        elif reqsize == "10M_100M":
            return 100*1024*1024
        elif reqsize == "100M_1G":
            return 1024*1024*1024
        elif reqsize == "1G_PLUS":
            return 1024*1024*1024
        else:
            assert False

    def estimate_bytes_from_reqsize_histogram(self, file_info, op):
        """
        Sometimes the phase has no information about the number
        of bytes that were accessed. In that case, we may use
        this function to estimate it from the histogram of
        request sizes.

        Parameters
        ----------
        file_info : CounterSet
            contains all counters collected from the trace
            file regarding accesses from one rank (or all
            ranks together) to a single file
        op : str
            the operation we are considering for this phase
            read or write.

        Returns
        -------
        int
            the estimated number of bytes, which could
            be 0 if we did not find information on the
            histogram of request sizes.
        """
        ret = 0
        sizes = ["0_100", "100_1K", "1K_10K", "10K_100K",
            "100K_1M", "1M_4M", "4M_10M", "10M_100M",
            "100M_1G", "1G_PLUS"]
        for size in sizes:
            ret += file_info.value_or_zero("CP_SIZE_"+op.upper()+"_"+size)*self.get_bytes_from_str(size)
            ret += file_info.value_or_zero("CP_SIZE_"+op.upper()+"_AGG_"+size)*self.get_bytes_from_str(size)
        return ret

    def parse_counters(self, file_info, op, api, threshold, nproc):
        """
        parse the counters and estimate all aspects of this
        phases' access pattern

        Parameters
        ----------
        file_info : CounterSet
            contains all counters collected from the trace
            file regarding accesses from one rank (or all
            ranks together) to a single file
        op : str
            the operation (or type) of this phase (must be
            "read" or "write")
        api :  str
            the api used for this phase (must be "posix"
            or "mpiio")
        threshold : float
            How many of the requests of a phase can be of
            other spatialities for us to consider the
            spatiality of the majority to the the
            spatiality of the whole phase. In other words,
            (1-threshold)*nreqs is  how much of the
            accesses must be sequential/consecutive/
            collective/etc for the whole phase to be
            considered that.
            0 <= threshold <= 1
        nproc : int
            the number of processes running this application

        Returns
        -------
        boolean :
            True if we could successfully estimate the
            phase, false otherwise.
        """
        if self.darshan == 3:
            assert api == "posix" or api == "mpiio"
            return self.parse_counters_darshan3(file_info, op, api, threshold, nproc)
        else:
            return self.parse_counters_darshan2(file_info, op, threshold, nproc)

    def parse_counters_darshan2(self, file_info, op, threshold,nproc):
        """
        see parse_counters
        """
        self.bytes = file_info.value_or_zero("CP_BYTES_"+read_or_written(op))
        if self.bytes == 0:
            #I found some cases where bytes is reported as
            #zero, but we have information in the histogram
            #of request sizes. That happened in cases where
            #multiple processes were involved in collective
            #operations, but for some reason darshan
            #reported the access separately to each rank.
            #Maybe because they are a subset of -1 ?
            self.bytes = self.estimate_bytes_from_reqsize_histogram(file_info, op)
        if self.bytes == 0:    #if no bytes were read/written,
                #we don't create this phase
            return False
        assert (op == "write") or (op == "read")
        self.type = op
        #find out how many processes are involved, the I/O time,
        #and the variance on the amount of data per process
        posix_time = file_info.value_or_zero("CP_F_POSIX_"+op.upper()+"_TIME")
        mpi_time = file_info.value_or_zero("CP_F_MPI_"+op.upper()+"_TIME")
        if posix_time > mpi_time:
            self.api = "posix"
        else:
            self.api = "mpiio"
        self.rank = file_info.get("rank")
        if self.rank == -1: #shared file
            self.files = "shared"
            self.subset_mode = nproc
            self.subset_avg = nproc
            self.time = file_info.value_or_zero("CP_F_SLOWEST_RANK_TIME")
            if self.time == 0:
                #this happened a few times, a mpi-io
                #phase without the information on the
                #slowest rank.
                duration = file_info.value_or_zero("CP_F_"+op.upper()+"_END_TIMESTAMP") - file_info.value_or_zero("CP_F_"+op.upper()+"_START_TIMESTAMP")
                if (duration <= 0) and (mpi_time > 0):
                    self.time = mpi_time
                elif (duration > 0) and (mpi_time <= 0):
                    self.time = duration
                elif (duration > 0) and (mpi_time > 0):
                    if duration <= mpi_time:
                        self.time = duration
                    else:
                        self.time = mpi_time
                else:
                    self.time = 0
            assert self.time > 0
            self.time_var = file_info.value_or_zero("CP_F_VARIANCE_RANK_TIME")
            self.bytes_var = file_info.value_or_zero("CP_F_VARIANCE_RANK_BYTES")
        else: #single rank
            self.files = "unique"
            self.subset_mode = 1
            self.subset_avg = 1
            if self.api == "posix":
                self.time = posix_time
            else:
                self.time = mpi_time
            self.bytes_var = 0
            self.time_var = 0
        #find out the start and the end of the phase
        self.start = file_info.get("CP_F_"+op.upper()+"_START_TIMESTAMP")
        self.end = Phase.verify_end(self.start, file_info.get("CP_F_"+op.upper()+"_END_TIMESTAMP"), self.time)
        #find out the number of requests
        col = file_info.value_or_zero("CP_COLL_"+op.upper()+"S")
        ind = file_info.value_or_zero("CP_INDEP_"+op.upper()+"S")
        split = file_info.value_or_zero("CP_SPLIT_"+op.upper()+"S")
        col += split #we assume split operations are collective
        if self.api == "posix":
            self.reqs = file_info.value_or_zero("CP_POSIX_F"+op.upper()+"S")
            self.reqs += file_info.value_or_zero("CP_POSIX_"+op.upper()+"S")
        else:
            self.reqs = col + ind
        assert self.reqs > 0
        #find out the access pattern
        cons = file_info.value_or_zero("CP_CONSEC_"+op.upper()+"S")
        seq = file_info.value_or_zero("CP_SEQ_"+op.upper()+"S")
        self.spatiality = self.decide_spatiality(cons, seq, col, threshold)
        return True


    def parse_counters_darshan3(self, file_info, op, api, threshold,nproc):
        """
        see parse_counters
        """
        assert (op == "write") or (op == "read")
        assert (api == "posix") or (api == "mpiio")
        self.bytes = file_info.value_or_zero(api.upper()+"_BYTES_"+read_or_written(op))
        if self.bytes == 0:    #if no bytes were read/written,
                #we don't create this phase
            return False
        self.type = op
        self.api = api
        #find out how many processes are involved, the I/O time,
        #and the variance on the amount of data per process
        self.rank = file_info.counters["rank"]
        if self.rank == -1: #shared file
            self.files = "shared"
            self.subset_mode = nproc
            self.subset_avg = nproc
            self.time = file_info.value_or_zero(api.upper()+"_F_"+op.upper()+"_TIME") / nproc
            assert self.time > 0
            self.time_var = 0
            self.bytes_var = file_info.value_or_zero(api.upper()+"_F_VARIANCE_RANK_BYTES")
        else: #single rank
            self.files = "unique"
            self.subset_mode = 1
            self.subset_avg = 1
            self.time = file_info.value_or_zero(api.upper()+"_F_"+op.upper()+"_TIME")
            self.bytes_var = 0
            self.time_var = 0
        #find out the start and the end of the phase
        self.start = file_info.counters[api.upper()+"_F_"+op.upper()+"_START_TIMESTAMP"]
        self.end = Phase.verify_end(self.start, file_info.counters[api.upper()+"_F_"+op.upper()+"_END_TIMESTAMP"], self.time)
        #find out the number of requests and the spatiality
        if self.api == "posix":
            self.reqs = file_info.value_or_zero("POSIX_"+op.upper()+"S")
            assert self.reqs > 0
            cons = file_info.value_or_zero("POSIX_CONSEC_"+op.upper()+"S")
            seq = file_info.value_or_zero("POSIX_SEQ_"+op.upper()+"S")
            self.spatiality = self.decide_spatiality_posix(cons, seq, threshold)
        else:
            col = file_info.value_or_zero("MPIIO_COLL_"+op.upper()+"S")
            ind = file_info.value_or_zero("MPIIO_INDEP_"+op.upper()+"S")
            split = file_info.value_or_zero("MPIIO_SPLIT_"+op.upper()+"S")
            nb = file_info.value_or_zero("MPIIO_NB_"+op.upper()+"S")
            assert split == 0
            assert nb == 0
#            col += split #we assume split operations are collective
            self.reqs = col + ind
            assert self.reqs > 0
            self.spatiality = self.decide_spatiality_mpiio(col, threshold)
        return True


    @staticmethod
    def verify_end(start,end, time):
        """
        Verify that time, start, and end make
        sense (the time is not longer than end - start by
        more than .1 seconds). Return the proper value
        for the end of the phase
        """
        duration = end - start
        #assert (time <= duration) or (time - duration <= 0.1)   in practice it sometimes isn't true...
#        if (time > duration) and (time - duration > 0.1):
#            print("Warning: Phase with end - start "+str(duration)+" and time "+str(time))
        if end >= (start + time):
            return end
        else:
            return start + time

    def decide_spatiality(self, cons, seq, col, threshold):
        if self.api == "posix":
            return self.decide_spatiality_posix(cons, seq, threshold)
        else:
            return self.decide_spatiality_mpiio(col, threshold)

    def decide_spatiality_posix(self, cons, seq, threshold):
        """
        Decide the spatiality of the phase from the number of
        consecutive and sequential requests

        Returns
        -------
        str
            contiguous, sequential, or random
        """
        target = self.reqs*(1.0 - threshold)
        if (seq >= target) or (seq == (self.reqs-1)) or (self.reqs == 1):
            if (cons >= target) or (cons == (self.reqs-1)) or (self.reqs == 1):
                spa = "contiguous"
            else:
                spa = "sequential"
        else:
            spa = "random"
        assert ((seq >= target) or (cons < target)) #could we have consecutive non-sequential???
        return spa

    def decide_spatiality_mpiio(self, col, threshold):
        """
        Decide the spatiality of the phase from the number of
        collective requests

        Returns
        -------
        str
            contiguous or unknown
        """
        target = self.reqs*(1.0 - threshold)
        if (col >= target) or (col == (self.reqs - 1)):
            spa = "contiguous"
        else:
            spa = "unknown"
        return spa

    def starts_within_1s(self, end):
        """
        Answers if the start of this phase happens at most 1s
        after the provided end timestamp
        """
        if self.start <= end:
            return True
        elif (self.start - end) <= 1:
            return True
        else:
            return False

    def compatible(self, other):
        """
        function used to verify if phases are compatible enough
        to be combined. They are compatible when they have the
        same api, type, spatiality, and file strategy (i.e. the
        same access pattern). This function does NOT
        verify the time aspect, the time_compatible function
        must be used for that.

        Parameters
        ----------
        other : Phase
            the other phase that we want to compare with
            this one

        Returns
        -------
        boolean
            are they compatible?
        """
        if (self.api != other.api) or (self.type != other.type):
            return False
        else:
            ret = self.spatiality == other.spatiality
            if not ret:
            #we decided that posix phases of a single request are
            #contiguous, but that is arbitrary, it does not
            #really mean anything. So we accept different
            #spatialities if one of the phases is of a
            #single request (and they are posix)
                if self.api == "posix":
                    ret = (self.reqs == 1) or (other.reqs == 1)
            if ret: #separated if because we change ret
                #inside the previous if
                return self.files == other.files
            return False

    def get_rank_number(self, nproc):
        """
        Returns the number of ranks involved in this phase
        """
        if self.rank == -1:
            return nproc
        elif self.rank < 0:
            return -self.rank
        else:
            return 1

    @staticmethod
    def use_all_ranks(rank_nb, nproc, threshold):
        """
        Given a number of ranks involved in the phase, the
        total number of processes involved in the application,
        and a threshold, answers if the number of ranks
        involved in the phase is enough for it to be considered
        all processes (so rank can be set to -1). Threshold is
        a percentual tolerance, the rank number can be slightly
        smaller than the total number of processes and still be
        considered -1.
        """
        if rank_nb >= ((1.0 - threshold)*nproc):
            return True

    def get_information(self):
        ret = str(self.start)+";"+str(self.end)+";"
        #for the operation type we use W for write and R for
        #read (to save space in the output file)
        if self.type == "write":
            ret += "W"
        elif self.type == "read":
            ret += "R"
        else:
            assert False
        ret += ";"+str(self.rank)+";"+str(self.bytes)+";"
        ret += str(self.time)+";"
        #for the api we use P for posix and M for mpiio
        if self.api == "posix":
            ret += "P"
        elif self.api == "mpiio":
            ret += "M"
        else:
            assert False
        ret += ";"+str(self.reqs)+";"
        #for the spatiality we use S for sequential, C for
        #contiguous, R for random and U for unknown
        if self.spatiality == "sequential":
            ret += "S"
        elif self.spatiality == "contiguous":
            ret += "C"
        elif self.spatiality == "random":
            ret += "R"
        elif self.spatiality == "unknown":
            ret += "U"
        else:
            assert False
        ret += ";"
        #for the file strategy we use U for unique and S for
        #shared
        if self.files == "unique":
            ret += "U"
        elif self.files == "shared":
            ret += "S"
        else:
            assert False
        ret += ";"+str(self.filenb)+";"+str(self.time_var)+";"
        ret += str(self.bytes_var)+";"+str(self.subset_mode)
        ret += ";"+str(self.subset_avg)+";"
        ret += str(self.subset_var)
        return ret


