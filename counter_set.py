#this simple function is here only to facilitate the creation of the
#long list of counters we will collect from the darshan traces
def make_list_of_counters():
    ret = {}
    #darshan version = 3
    ret[3] = ["POSIX_READS", "POSIX_WRITES", "POSIX_BYTES_READ",
    "POSIX_BYTES_WRITTEN", "POSIX_CONSEC_READS",
    "POSIX_CONSEC_WRITES", "POSIX_SEQ_READS",
    "POSIX_SEQ_WRITES", "POSIX_F_READ_START_TIMESTAMP",
    "POSIX_F_WRITE_START_TIMESTAMP",
    "POSIX_F_READ_END_TIMESTAMP",
    "POSIX_F_WRITE_END_TIMESTAMP", "POSIX_F_READ_TIME",
    "POSIX_F_WRITE_TIME", #"POSIX_F_SLOWEST_RANK_TIME",
    "POSIX_F_VARIANCE_RANK_TIME",
    "POSIX_F_VARIANCE_RANK_BYTES", "MPIIO_INDEP_READS",
    "MPIIO_INDEP_WRITES",
    "MPIIO_COLL_READS", "MPIIO_COLL_WRITES",
    "MPIIO_SPLIT_READS", "MPIIO_SPLIT_WRITES",
    "MPIIO_NB_READS", "MPIIO_NB_WRITES",
    "MPIIO_BYTES_READ", "MPIIO_BYTES_WRITTEN",
    "MPIIO_F_READ_START_TIMESTAMP",
    "MPIIO_F_WRITE_START_TIMESTAMP",
    "MPIIO_F_READ_END_TIMESTAMP",
    "MPIIO_F_WRITE_END_TIMESTAMP",
    "MPIIO_F_READ_TIME", "MPIIO_F_WRITE_TIME",
    #"MPIIO_F_SLOWEST_RANK_TIME",
    "MPIIO_F_VARIANCE_RANK_TIME",
    "MPIIO_F_VARIANCE_RANK_BYTES"]
    #darshan version = 2
    access_sizes = ["0_100", "100_1K", "1K_10K", "10K_100K", "100K_1M",
                   "1M_4M", "4M_10M", "10M_100M", "100M_1G", "1G_PLUS"]
    ret[2] = ["CP_POSIX_READS", "CP_POSIX_WRITES", "CP_POSIX_FREADS",
            "CP_POSIX_FWRITES", "CP_COLL_READS",  "CP_COLL_WRITES",
            "CP_INDEP_READS", "CP_INDEP_WRITES", "CP_SPLIT_READS",
            "CP_SPLIT_WRITES", "CP_NB_READS", "CP_NB_WRITES",
        "CP_BYTES_READ", "CP_BYTES_WRITTEN", "CP_CONSEC_READS",
        "CP_CONSEC_WRITES", "CP_SEQ_READS", "CP_SEQ_WRITES",
        "CP_F_READ_START_TIMESTAMP", "CP_F_WRITE_START_TIMESTAMP",
            "CP_F_READ_END_TIMESTAMP", "CP_F_WRITE_END_TIMESTAMP",
            "CP_F_VARIANCE_RANK_BYTES",
            "CP_F_SLOWEST_RANK_TIME", "CP_F_VARIANCE_RANK_TIME"]
    for api in ["POSIX", "MPI"]:
        for op in ["READ", "WRITE"]:
            ret[2].append("CP_F_"+api+"_"+op+"_TIME")
    sizes = ["0_100", "100_1K", "1K_10K", "10K_100K", "100K_1M",
        "1M_4M", "4M_10M", "10M_100M", "100M_1G", "1G_PLUS"]
    for reqsize in sizes:
        for t in ["", "_AGG"]:
            for op in ["READ", "WRITE"]:
                 ret[2].append("CP_SIZE_"+op+t+"_"+reqsize)
    return ret

#the list containing the counters we look for in the darshan trace
relevant_counters = make_list_of_counters()

class CounterSet:
    """
    A collection of counters associated with an I/O phase to a rank
    (or all ranks together) to a single file handle.

    ...

    Attributes
    ----------
    counters : dictionary of key string and value int or float
        to each counter among the ones we are interested on,
        associates the value collected from the darshan trace
    darshan : int
        the darshan version used to generate and parse the
        trace files

    Methods
    -------
    len() :
        returns the number of counters associated with values
    set(key, value):
        make an association of counter and value if that is
        valid, i.e. if the key is in our list of relevant
        counters (or it is "rank"), and the value is different
        from 0 and from -1 (except for the key "rank", which
        can be associated with -1).
    value_or_zero(key) :
        if the provided key is present in our collected
        counters, return the value associated with it. If it is
        not, return 0.
    """
    def __init__(self, darshan):
        """
        Parameters
        ----------
        darshan : int
            Darshan version used to generate and parse the
            trace
        """
        self.counters = {}
        self.darshan = darshan

    def len(self):
        return len(self.counters.keys())

    def set(self, key, value):
        if self.darshan == 2:
            #In the Intrepid data set, in a trace from
            #January 24 2013, several phases were reported
            #for the same handle + rank one right after the
            #other. When that happens, we don't detect the
            #end of one phase and the start of the next
            #one, so we would get an error here because we
            #were trying to set keys that were already
            #defined. Hence we have to combine them on the
            #fly
            if key in self.counters:
                self.incorporate_key(key, value)
        else: #darshan == 3
            assert not (key in self.counters)
        if (key in relevant_counters[self.darshan]) or (key == "rank"):
            if (key == "rank") or ("TIMESTAMP" in key) or ((value != 0) and (value != -1)):
                self.counters[key] = value

    def value_or_zero(self, key):
        if key in self.counters:
            return self.counters[key]
        else:
            return 0

    def __str__(self):
        return str(self.counters)

    def incorporate_key(self, counter, value):
        if self.darshan == 3:
            assert not (counter in self.counters)
            self.counters[counter] = value
        else: # self.darshan == 2
            #see the comments in the set() method
            if not (counter in self.counters):
                self.counters[counter] = value
            elif "START_TIMESTAMP" in counter:
                self.counters[counter] = min(self.counters[counter], value)
            elif ("END_TIMESTAMP" in counter) or ("VARIANCE" in counter):
                self.counters[counter] = max(self.counters[counter], value)
            else:
                self.counters[counter] += value


    def incorporate(self,other):
        """
        incorporate another CounterSet object into this one

        Parameters
        ----------
        other : CounterSet
            the set of counters that collide with this one
            and that will be tossed after this call.
        """
        for counter in other.counters:
            self.incorporate_key(counter, other.counters[counter])


