from job_info import JobInformation
from counter_set import CounterSet
from phase import Phase
from phase_collection import PhaseCollection

from datetime import datetime

class Trace:
    """
    Contains information collected from a darshan trace, i.e.
    all I/O activity of a job.

    ...

    Attributes
    ----------
    filename : str
        the name of the file containing the counters for this
        trace
    info : JobInformation object   (TODO useless?)
        the general information about this job (number of
        processes, execution time, etc)
    phases : List of Phase objects
        the list of all I/O phases of this job
    verbose : boolean
        parameter set in the constructor to say if we should
        print intermediate results or not (they are useful
        when verifying results but now when we are parsing
        hundreds of files)
    darshan : int
        the darshan version used to generate and parse the
        traces

    Methods
    -------
    print_phases():
        print all phases that were estimated from this trace
    """
    def __init__(self, filename, threshold, iterative=False, verbose = False, darshan=3, parse_in_2024=False):
        """
        Open the file, read it, parse all lines to collect all
        the counters we want, and estimate the I/O phases.

        Parameters
        ----------
        filename : str
            file name (with path) to read the trace file
            containing information about this job
        threshold : float
            How many of the requests of a phase can be of
            other spatialities for us to consider the
            spatiality of the majority to the the
            spatiality of the whole phase. In other words,
            (1-threshold)*nreqs is  how much of the
            accesses must be sequential/consecutive/
            collective/etc for the whole phase to be
            considered that.
            0 <= threshold <= 1
        iterative : boolean
            should we ask the user to press Enter before
            treating the next file handle?
        verbose : boolean
            should we print intermediate results?
        """
        self.filename = filename
        self.verbose = verbose
        self.darshan = darshan
        self.phases = []
        self.parse_in_2024 = parse_in_2024
        #read and parse the file
        arq = open(filename, "r")
        contents = arq.readlines()
        arq.close()
        self.info = JobInformation(self.darshan, parse_in_2024, filename, filename.split('/')[-1].split('.')[0])
        perfile = self.parse_file(contents)
        if len(self.info.info) == 0:  #we have a few files that
                #darshan simply cannot parse, it gives
                #empty parsed files
            return
        #in some cases there is missing information
        if not "app" in self.info.info:
            self.info.info["app"] = "unknown"
        if not "user" in self.info.info:
            self.info.info["user"] = "unknown"
        if not "nodes" in self.info.info:
            self.info.info["nodes"] = -1
        if not "procs" in self.info.info:
            self.info.info["procs"] = -1
        self.debug(self.info)
        if parse_in_2024:
            self.phases = self.simple_phase_parser(perfile, self.info.info["start"])
            return
        #now process the obtained counters (in perfile,
        #organized by handle and by rank) to estimate phases
        self.counter = 0
        self.debug("files accessed: "+str(perfile.keys()))
        for handle in perfile:
            new_phases = self.generate_phases_list(perfile[handle], threshold, handle)
            self.debug([str(a) for a in new_phases])
            self.phases.extend(new_phases)
            if iterative:
                input("Press Enter to continue")
        #combine phases that should be reported together
        self.phases = sorted(self.phases, key = lambda x: x.start)
        self.combine_phases(threshold)
        total_time = 0.0
        total_bytes = 0
        for phase in self.phases:
            total_bytes += phase.bytes
            total_time += phase.time

        #some fixes to weird darshan reports
        if not ("bytes" in self.info.info):
            self.info.info["bytes"] = total_bytes
        elif total_bytes != self.info.info["bytes"]:
            self.debug("the amount of data does not match (" + str(self.info.info["bytes"]) + " vs "+str(total_bytes)+")")
            self.info.info["bytes"] = total_bytes
        if not ("io_time" in self.info.info):
            self.info.info["io_time"] = total_time
        elif total_time != self.info.info["io_time"]:
            self.debug("the io time does not match (" +str(self.info.info["io_time"]) + " vs "+str(total_time)+")")
            self.info.info["io_time"] = total_time
        if total_time > 0:
            self.debug("performance of "+str(float(total_bytes/1024.0/1024.0)/total_time)+" MB/s" )

    def combine_phases(self, threshold):
        """
        Go through all phases (in self.phases) that were
        estimated from the counters collected from the trace
        file, and combine phases that are too similar and
        happen too close in time. This function also updates
        the number of shared and unique files in the global
        information of the application.
        """
        i = 0
        while i < (len(self.phases)-1):
            #to each phase, we will compare to all others
            #that start within 1s, and for the ones that
            #are compatible, we remove them from the list,
            #and then make self.phases[i] into a
            #PhaseCollection of all phases that are to be
            #combined.
            j = i+1
            end = self.phases[i].end
            first = self.phases[i]
            while (j < len(self.phases)) and self.phases[j].starts_within_1s(end):
                if (not isinstance(self.phases[i], PhaseCollection)) and self.phases[i].compatible(self.phases[j]):
                    collection = PhaseCollection(self.phases[i].api, self.phases[i].type)
                    collection.add_phase_independent(self.phases[i], self.info.info["nprocs"])
                    collection.add_phase_independent(self.phases[j], self.info.info["nprocs"])
                    self.phases[i] = collection
                    end = max(end, self.phases[j].end)
                    del self.phases[j]
                elif (isinstance(self.phases[i], PhaseCollection)) and (first.compatible(self.phases[j])):
                    self.phases[i].add_phase_independent(self.phases[j], self.info.info["nprocs"])
                    end = max(end, self.phases[j].end)
                    del self.phases[j]
                else:
                    j+=1
            i+=1
        #now we have a list of phases, where each phase may
        #actually be a list of phases that have to be merged.
        #So we go through this list doing the required merges
        #to restore it as a simple list of phases.
        self.debug("\nMERGING\n")
        i = 0
        file_count = {"unique": 0, "shared": 0}
        while i< len(self.phases):
            if isinstance(self.phases[i], PhaseCollection):
                self.debug("TO MERGE: ")
                for phase in self.phases[i].phase_list:
                    self.debug(phase)
                    #if len(phase.rank_list) > 0:
                    #    print(phase.rank_list)
                self.debug("RESULT: ")
                self.phases[i] = self.phases[i].collapse_independent(threshold, self.info.info["nprocs"], self.darshan)
                self.debug(self.phases[i])
            else:
                self.debug(self.phases[i])
            self.debug("-----------------")
            assert self.phases[i].files in file_count
            file_count[self.phases[i].files] += self.phases[i].filenb
            i += 1
        #verify (and fix) our counters for number of unique and
        #shared files accessed by the job
        for key in file_count:
            if not ((key+"_files") in self.info.info):
                self.info.info[key+"_files"] = file_count[key]
            elif file_count[key] != self.info.info[key+"_files"]:
                self.debug("Warning! Number of "+key+" files does not match ("+str(file_count[key])+" vs "+str(self.info.info[key+"_files"])+")")
                self.info.info[key+"_files"] = file_count[key]

    def parse_file_darshan3(self,contents):
        file_info = {}
        metadata_files = 0
        curfile = ""
        state = "loose_counters"
        for line in contents:
            parsed_line=line.split()
            if len(parsed_line) == 0: #skip
                continue
            if state == "loose_counters": #we will obtain
                #global information about the job from
                #some counters
                change = self.info.decode_line(parsed_line, line)
                if change:
                    state = "before_table"
            elif state == "before_table": #we need to skip
                #some lines before arriving at the
                #table of counters
                if parsed_line[0] == "#<module>":
                    state = "read_table"
            elif state == "read_table": #we arrived at the
                #table of counters, from where we will
                #obtain information to estimate the I/O
                #phases
                if "# description of" in line:
                    state = "loose_counters"
                    #store the last counters read
                    if(curfile != ""):
                        if not (curfile in file_info):
                            file_info[curfile]={}
                        if currank in file_info[curfile]:
                            file_info[curfile][currank].incorporate(curfile_info)
                        else:
                            curfile_info.set("rank", currank)
                            file_info[curfile][currank] = curfile_info
                        curfile = ""
                else: #get counters from the table
                    if curfile == "": #first file
                            #from the table
                        curfile = parsed_line[5]
                        currank = int(parsed_line[1])
                        curfile_info = CounterSet(self.darshan)
                    if (parsed_line[5] != curfile) or (int(parsed_line[1])!=currank):
                        #we are done reading
                        #counters for this
                        #file+rank, wrap up
                        #before moving to the
                        #next
                        if not (curfile in file_info):
                            file_info[curfile]={}
                        if currank in file_info[curfile]:
                            file_info[curfile][currank].incorporate(curfile_info)
                        else:
                            curfile_info.set("rank", currank)
                            file_info[curfile][currank] = curfile_info
                        curfile = parsed_line[5]
                        currank = int(parsed_line[1])
                        curfile_info = CounterSet(self.darshan)
                    assert len(parsed_line) == 8
                    curfile_info.set(parsed_line[3], float(parsed_line[4]))
            else: #impossible state!
                assert False
        #end for line in lines
        return file_info

    def parse_file_darshan2(self,contents):
        file_info = {}
        reading_table = False
        metadata_files = 0
        curfile = -1
        for line in contents:
            parsed_line=line.split()
            if not reading_table:
                reading_table = self.info.decode_line(parsed_line, line)
            else: #we are reading the table of counters
                if curfile == -1: #first file
                    curfile = parsed_line[1]
                    currank = int(parsed_line[0])
                    curfile_info = CounterSet(self.darshan)
                if (parsed_line[1] != curfile) or (str(currank) != parsed_line[0]): #we are done reading counters
                         #about this file+rank, wrap up before moving
                        #on to the next
                    if curfile_info.len() > 0:  #if all counters for this
                        #file are 0 or unknown, it is
                        #not a read/write access
                        #(usually only metadata)
                        if not (curfile in file_info):
                            file_info[curfile]={}
                        if currank in file_info[curfile]:
                            file_info[curfile][currank].incorporate(curfile_info)
                        else:
                            curfile_info.set("rank", currank)
                            file_info[curfile][currank] = curfile_info
                    else:
                        assert currank != -1 #can we have shared-file metadata-only access?
                        metadata_files += 1
                    if len(parsed_line) == 2: #we are done reading the table
                        reading_table = False
                        continue
                    curfile = parsed_line[1]
                    currank = int(parsed_line[0])
                    curfile_info = CounterSet(self.darshan)
                assert len(parsed_line) == 7
                if currank != int(parsed_line[0]):
                    print(parsed_line)
                assert currank == int(parsed_line[0])
                curfile_info.set(parsed_line[2], float(parsed_line[3]))
        if metadata_files > 0:
            self.info.remove_metadata_files(metadata_files)
        return file_info

    def parse_file(self,contents):
        """
        Read information about the job from the trace file.
        Fills self.info with job-level information and return
        the counters collected per file handle (which have to
        be further processed later to become the I/O phases.

        Parameters
        ----------
        contents : str
            The whole trace file contents, obtained with
            readlines() from the file.

        Returns
        -------
        dictionary<str, dictionary<int, CounterSet>>
            associate one dictionary per file handle in the
            trace file. In each of these dictionaries,
            associate one CounterSet object to each rank,
            containing all counters obtained from the trace
            about the access from this rank to this file.
        """
        if self.darshan == 3:
            return self.parse_file_darshan3(contents)
        else: #darshan == 2
            return self.parse_file_darshan2(contents)

    def simple_phase_parser(self, perfile, job_start):
        assert self.darshan == 2
        ret = []
        for handle in perfile:
            for rank in perfile[handle]:
                #this is one access reported by darshan
                counters = perfile[handle][rank]
                this_info= {
                    "read": counters.value_or_zero("CP_BYTES_READ"),
                    "read_duration": counters.value_or_zero("CP_F_POSIX_READ_TIME") + counters.value_or_zero("CP_F_MPI_READ_TIME"),
                    "written": counters.value_or_zero("CP_BYTES_WRITTEN"),
                    "write_duration": counters.value_or_zero("CP_F_POSIX_WRITE_TIME") + counters.value_or_zero("CP_F_MPI_WRITE_TIME")
                }
                read_start= counters.value_or_zero("CP_F_READ_START_TIMESTAMP")
                read_end = counters.value_or_zero("CP_F_READ_END_TIMESTAMP")
                if read_end == 0 and read_start == 0:
                    this_info["read_start_ts"] = None
                    this_info["read_end_ts"] = None
                else:
                    this_info["read_start_ts"] = datetime.fromtimestamp(read_start + job_start)
                    this_info["read_end_ts"] = datetime.fromtimestamp(read_end + job_start)
                write_start= counters.value_or_zero("CP_F_WRITE_START_TIMESTAMP")
                write_end = counters.value_or_zero("CP_F_WRITE_END_TIMESTAMP")
                if write_end == 0 and write_start == 0:
                    this_info["write_start_ts"] = None
                    this_info["write_end_ts"] = None
                else:
                    this_info["write_start_ts"] = datetime.fromtimestamp(write_start + job_start)
                    this_info["write_end_ts"] = datetime.fromtimestamp(write_end + job_start)
                if this_info["read_duration"] > 0:
                    this_info["read_speed"] = this_info["read"]/this_info["read_duration"]
                else:
                    this_info["read_speed"] = 0
                if this_info["write_duration"] > 0:
                    this_info["write_speed"] = this_info["written"]/this_info["write_duration"]
                else:
                    this_info["write_speed"] = 0

                if (this_info["read"] > 0 or this_info["written"] > 0) and (this_info["read_duration"] > 0 or this_info["write_duration"] > 0):
                    ret.append(this_info)
        return ret

    def generate_phases_list(self, file_info, threshold, handle):
        """
        Parameters
        ----------
        file_info : dictionary<int, CounterSet>
            dictionary describing all accesses to a given
            file handle. To each rank, the counters
            collected about the access from that rank to
            the file.
        threshold : float
            How many of the requests of a phase can be of
            other spatialities for us to consider the
            spatiality of the majority to the the
            spatiality of the whole phase. In other words,
            (1-threshold)*nreqs is  how much of the
            accesses must be sequential/consecutive/
            collective/etc for the whole phase to be
            considered that.
            When collapsing phases where multiple different
            ranks accessed the same file handle, it says
            how many processes can be out of this phase for
            it to still be considered -1 (all ranks).
            0 <= threshold <= 1
        handle : str
            The file handle

        Returns
        -------
        List<Phase> :
            a list of Phase objects with the estimated
            I/O phases of the job that access the same
            handle
        """
        #verify if there are multiple ranks that accesses the same file
        rank_nb = len(file_info.keys())
        self.counter += 1
        self.debug(self.counter)
        self.debug(handle)
        assert rank_nb > 0
        if rank_nb == 1:
            #for this file we collected information connected to only
            #one rank (which could be -1 representing all ranks)
            rank = next(iter(file_info.keys()))
            return self.generate_phases_singlerank(file_info[rank], threshold, handle)
        else:
            phases = []
            self.debug("Found a file accessed by multiple ranks! "+str(rank_nb) + " from "+str(self.info.info["nprocs"]))
            #parse the phases of each rank and separate them by api and op
            collection = {}
            for api in ["posix", "mpiio"]:
                collection[api] = {}
                for op in ["write", "read"]:
                    collection[api][op] = PhaseCollection(api, op)
            for rank in file_info:
                these_phases = self.generate_phases_singlerank(file_info[rank], threshold, handle)
                for this_phase in these_phases:
                    collection[this_phase.api][this_phase.type].add_phase_shared(this_phase)
            #collapse the phases of read/write and mpiio/posix.
            #In Darshan 2, we don't have enough separate
            #counters to separate posix and mpiio, so we just
            #estimate which is it by the amount of data written.
            #However, in darshan 3 we can do better.
            if self.darshan == 3:
                #collapse them into up to four phases: read/
                #write posix/mpiio
                for op in ["write", "read"]:
                    for api in ["posix", "mpiio"]:
                        this_collection = collection[api][op].collapse_shared(threshold, self.info.info["nprocs"], self.darshan)
                        if this_collection.bytes > 0:
                            phases.append(this_collection)
            else:
                for op in ["write", "read"]:
                    posix_phase = collection["posix"][op].collapse_shared(threshold, self.info.info["nprocs"], self.darshan)
                    mpiio_phase = collection["mpiio"][op].collapse_shared(threshold, self.info.info["nprocs"], self.darshan)
                    #if we have both, we take the one
                    #responsible for most of the time
                    if posix_phase.time > mpiio_phase.time:
                        if posix_phase.bytes > 0:
                            phases.append(posix_phase)
                    else:
                        if mpiio_phase.bytes > 0:
                            phases.append(mpiio_phase)
            return phases


    def generate_phases_singlerank(self, file_info, threshold, handle):
        """
        Converts the a set of counters coming from the trace
        file into an estimated I/O phase

        Parameters
        ----------
        file_info : CounterSet
            object with values to all counters we collected
            from the trace file
        threshold : float
            How many of the requests of a phase can be of
            other spatialities for us to consider the
            spatiality of the majority to the the
            spatiality of the whole phase. In other words,
            (1-threshold)*nreqs is  how much of the
            accesses must be sequential/consecutive/
            collective/etc for the whole phase to be
            considered that.
            0 <= threshold <= 1
        handle : str
            The file handle

        Returns
        -------
        List<Phase> :
            a list of Phase objects with the estimated I/O
            phases of the job where a given rank(s) access
            a given file handle
        """
        if self.darshan == 3:
            return self.generate_phases_singlerank_darshan3(file_info, threshold, handle)
        else:
            return self.generate_phases_singlerank_darshan2(file_Info, threshold, handle)


    def generate_phases_singlerank_darshan2(self, file_info, threshold, handle):
        """
        see generate_phases_siglerank
        """
        self.debug(file_info)
        ret = []
        for op in ["read", "write"]: #we will potentially
                #create two phases, one for reads and
                #another for writes.
            new = Phase(self.darshan)
            if new.parse_counters(file_info, op, "", threshold,self.info.info["nprocs"]):
                ret.append(new)
        return ret

    def generate_phases_singlerank_darshan3(self, file_info, threshold, handle):
        """
        see generate_phases_singlerank
        """
        self.debug(file_info)
        ret = []
        #sometimes a collective MPI-IO call will appear twice,
        #once for mpi-io and once for posix, with the same
        #amounts of data and both as -1. That happens for the
        #BT-IO benchmark. Investigating the issue I concluded
        #the posix counters correspond to the I/O operations
        #generated by the aggregator nodes. But that is not
        #what we want to report, because then we would be led
        #to believe the application accesses twice the amount
        #of data is actually does (and using POSIX, which maybe
        #it doesn't use). So after generating posix and mpiio
        #phases we check to see if they fit this profile, and
        #if they do we discard the posix one.
        for op in ["read", "write"]: #we will potentially
                #create four phases, read/write
                #posix/mpiio
            posix_phase = None
            mpiio_phase = None
            for api in ["posix", "mpiio"]:
                new = Phase(self.darshan)
                if new.parse_counters(file_info, op, api, threshold,self.info.info["nprocs"]):
                    if api == "posix":
                        posix_phase = new
                    else:
                        mpiio_phase = new
            if (posix_phase != None) and (mpiio_phase != None):
                #check to see if they fit that
                #situation I described above, of the
                #collective operation being reported
                #twice
                if (posix_phase.bytes == mpiio_phase.bytes) and (posix_phase.rank == mpiio_phase.rank) and (posix_phase.rank == -1) and (mpiio_phase.spatiality == "contiguous") and (posix_phase.files == mpiio_phase.files) and (posix_phase.files == "shared"):
                    #Im not testing timestamps here
                    #but maybe I should, need more
                    #tests to decide
                    print("WARNING: discarding POSIX phase because it seems a duplicate of MPI-IO phase to this file")
                    posix_phase = None
            if posix_phase != None:
                ret.append(posix_phase)
            if mpiio_phase != None:
                ret.append(mpiio_phase)
        return ret

    def debug(self, msg):
        """
        Print something if we are being verbose
        """
        if self.verbose:
            print(msg)

    def print_phases(self):
        """
        Used for debug purposes.
        """
        print("\n\n")
        print(self.info)
        for phase in self.phases:
            print(phase)

    def get_job_info(self):
        """
        Returns
        -------
        The job information in a line ready to be written to
        the output file, including the "\n"
        """
        if len(self.info.info) == 0:
            return ""
        return self.filename+";"+self.info.get_info()+"\n"

    def get_phases_info(self):
        """
        Returns
        -------
        The phases information in a string ready to be written
        to the output file, including the "\n"
        """
        ret = ""
        for phase in self.phases:
            ret += self.filename+";"+phase.get_information()+"\n"
        return ret

    def get_simple_trace(self):
        assert self.parse_in_2024
        if len(self.phases) == 0:
            return None
        ret = {
            "infos": self.info.get_info_as_dict()
        }
        if ret["infos"] is None:
            return None
        ret["module"] = {
            "read": sum([x["read"] for x in self.phases]),
            "written": sum([x["written"] for x in self.phases]),
            "read_time": sum([x["read_duration"] for x in self.phases]),
            "write_time": sum([x["write_duration"] for x in self.phases])
        }
        if ret["module"]["read"] == 0 and ret["module"]["written"] == 0:
            return None
        ret["access"] = self.phases
        return ret


