from numpy import sum,var,max,mean
from phase import Phase

def value_or_zero(dictionary, key):
    if key in dictionary:
        return dictionary[key]
    else:
        return 0

class PhaseCollection:
    """
    A collection of phases that should be reported as a single one.
    Usage: a PhaseCollection object will be built and then phases
    will be added to it. At the end, the collapse method will be
    called to build a single Phase out of it.
    This class will be used in two situations:
    1) multiple ranks accessed the same shared file, but Darshan
    reported it separately per rank. In that case, we want to build
    a single shared-file phase out of it.
    2) after estimating all phases from the trace file, we found
    some that share the same access pattern and happen really close
    in time, so they should be merged into a single phase.
    ...

    Attributes
    ----------
    api : string
        the api used for the access, i.e. posix or mpiio
    type : string
        the operation, i.e. read or write
    spatiality : a dictionary with key string and value int
        possible keys are "contiguous", "sequential", "random",
        and "unknown". Associated with each key are the number
        of requests coming from phases classified with that
        spatiality. That is only useful when manually combining
        phases that are to the same handle, but not necessarily
        with the same access pattern (case 1)
    start : float
        the timestamp of the beginning of the phase. It will be
        the minimum of all phases' beginnings
    end : float
        the timestamp of the end of the phase. It will be the
        maximum of all phases' ends.
    bytes : Dictionary<int>
        a dictionary containing the amount of bytes accessed in
        by each of the involved ranks
    time_dict : Dictionary<float>
        a dictionary containing, to each rank, the time it
        spent across the collection of phases.
    reqs : int
        the total number of requests issued by all phases
    last : Phase
        used so we can access fields that are not changed by
        being in a collection
    ranks : Dictionary<int>
        a dictionary containing, to each rank that participated
        in this collection of phases, the number of times it
        appeared (if we are combining phases in the case 2,
        some ranks might be involved in more than one phase)
    rank_count : Dictionary<int>
        a dictionary containing, to each number of ranks
        involved in individual phases, the number of phases
        where that number was observed
    phase_list : List<Phase>
        a list containing all phases in this collection, only
        used when combining independent phases. This is actually
        only used when debugging (because we'll want to print
        information about all phases being combined).
    filenb : int
        number of files being accessed in this phase. Only
        used for the case 2, where we are combining phases
        that access different files

    Methods
    -------
    add_phase_shared(Phase) :
        adds a phase from a single rank in this collection,
        which represents accesses from multiple ranks to the
        same file handle
    add_phase_independent(Phase) :
        adds an independently-estimated phase into this
        collection, which represents accesses to different
        files with similar access pattern and close in time
    collapse_shared(threshold, nproc) :
        builds a single phase out of this collection. This
        version of collapse_ is to be used when we are
        combining phases of single ranks to a shared file
    collapse_independent(threshold, nproc) :
        builds a single phas eout of this collection. This
        version of collapse is to be used when we are
        combining phases that are to different files

    """
    def __init__(self, api, op):
        """
        Parameters
        ----------
        api : string
            "posix" or "mpiio"
        op : string
            "read" or "write", the operation (or type)
        """
        assert (api == "posix") or (api == "mpiio")
        self.api = api
        assert (op == "write") or (op == "read")
        self.type = op
        self.spatiality= {}
        self.start = -1
        self.end = 0
        self.bytes = {}
        self.time_dict = {}
        self.reqs = 0
        self.ranks = {}
        self.phase_list = []
        self.filenb = 0
        self.rank_count = {}

    def add_phase_common(self, this_phase):
        """
        Some things in common between add_phase_independent and
        add_phase_shared. For internal use only
        """
        if (this_phase.start < self.start) or (self.start < 0):
            self.start = this_phase.start
        if (this_phase.end > self.end):
            self.end = this_phase.end
        self.reqs += this_phase.reqs

    def add_phase_independent(self, this_phase,nproc):
        """
        Adds a phase to this collection in the case 2, where
        phases access different files, but are all of similar
        access patterns

        Parameters
        ----------
        this_phase : Phase
            all phase information estimated from counters
        nproc : int
            the number of processes executing this job
        """
        self.phase_list.append(this_phase)
        self.add_phase_common(this_phase)
        assert this_phase.filenb > 0
        self.filenb += this_phase.filenb
        if this_phase.files == "unique":
            assert this_phase.rank >= 0
            if not this_phase.rank in self.time_dict:
                self.time_dict[this_phase.rank] = 0
                self.bytes[this_phase.rank] = 0
                self.ranks[this_phase.rank] = 0
            self.time_dict[this_phase.rank] += this_phase.time
            self.bytes[this_phase.rank] += this_phase.bytes
            self.ranks[this_phase.rank] += 1
        else:
            assert this_phase.rank < 0
            #when we have a shared file, we recover the
            #list of ranks involved in that phase, and then
            #count that time (the max) for all of them ir
            #our time_dict, so eventually if one rank is
            #involved in multiple shared-file phases in the
            #same collection that will increase the time of
            #the aggregated phase (because the slowest rank
            #will have a sum of times)
            ranklist = this_phase.rank_list
            if len(ranklist) == 0:
                assert this_phase.rank == -1
                ranklist = [i for i in range(nproc)]
                ranknb = nproc
            else:
                if this_phase.rank == -1:
                    ranknb = nproc
                    if len(ranklist) != ranknb: #this could be the case because we had *almost* all processes (but not all) and thus classified the phase as -1 nonetheless
                        ranklist = [i for i in range(nproc)]
                else:
                    ranknb = -this_phase.rank
                if len(ranklist) != ranknb:
                    print("PANIC!")
                    print(this_phase)
                    print(this_phase.rank_list)
                    print(ranklist)
                    print(ranknb)
                assert len(ranklist) == ranknb
            for rank in ranklist:
                if not rank in self.time_dict:
                    self.time_dict[rank] = 0
                    self.bytes[rank] = 0
                self.time_dict[rank] += this_phase.time
                self.bytes[rank] += this_phase.bytes
            if not ranknb in self.rank_count:
                self.rank_count[ranknb] =0
            self.rank_count[ranknb]+=1

    def add_phase_shared(self, this_phase):
        """
        Updates this phase collection with the inclusion of a
        new phase with information about the access of a single
        rank to the shared file handle

        Parameters
        ----------
        this_phase : Phase
            all phase information estimated from counters
        """
        self.add_phase_common(this_phase)
        assert(this_phase.rank >= 0)
        if not this_phase.rank in self.time_dict:
            self.time_dict[this_phase.rank] = 0
            self.bytes[this_phase.rank] = 0
            self.ranks[this_phase.rank] = 0
        self.time_dict[this_phase.rank] += this_phase.time
        self.bytes[this_phase.rank] += this_phase.bytes
        self.ranks[this_phase.rank] += 1
        if not (this_phase.spatiality in self.spatiality):
            self.spatiality[this_phase.spatiality] = 0
        self.spatiality[this_phase.spatiality] += this_phase.reqs
        self.last = this_phase


    def collapse_independent(self, threshold, nproc, darshan):
        """
        Parameters
        ----------
        threshold : float
            how many processes can be out of this phase for
            it to still be considered -1 (all ranks).
            0 <= threshold <= 1
        nproc : int
            how many processes executed this job
        darshan : int
            the darshan version used for this trace

        Returns
        -------
        Phase
            a phase that represents this whole collection
            together.
        """
        assert len(self.phase_list) > 0
        new = Phase(darshan)
        new.type = self.type
        new.api = self.api
        new.files = self.phase_list[0].files  #it is okay to
                #take from
                #one of the requests because they have
                #the same value in this field for sure
        new.filenb = self.filenb
        ranknb = len(self.time_dict) #the dictionary has one
                #entry per rank, so its length is the
                #number of ranks involved
        assert ranknb > 0
        if ranknb == 1:
            new.rank = self.phase_list[0].rank
        elif Phase.use_all_ranks(ranknb, nproc, threshold):
            new.rank = -1
        else:
            new.rank = - ranknb
        #some things depend on the type of access
        if new.files == "unique":
            #for spatiality we have to ignore phases of a single
            #request, because we don't consider those when
            #comparing phases for compatibility
            i = 0
            while i < len(self.phase_list):
                if self.phase_list[i].reqs > 1:
                    break
                i+=1
            if i >= len(self.phase_list): #if all are single-req phases
                i = 0 #just take whatever
            new.spatiality = self.phase_list[i].spatiality
        else:
            new.spatiality = self.phase_list[0].spatiality
            #calculate the avg and mode of the number of
            #processes involved in each phase of this
            #collection
            assert len(self.rank_count) > 0
            mode = 0
            mode_count = 0
            subset_sum=0
            rank_numbers = []
            for rank in self.rank_count:
                if self.rank_count[rank] > mode_count:
                    mode_count = self.rank_count[rank]
                    mode = rank
                subset_sum += rank*self.rank_count[rank]
                rank_numbers.extend([rank for i in range(self.rank_count[rank])])
            new.subset_mode = mode
#            new.subset_avg = subset_sum/len(self.phase_list)
            new.subset_avg = mean(rank_numbers)
            new.subset_var = var(rank_numbers)
        new.time = max(list(self.time_dict.values()))
        new.time_var = var(list(self.time_dict.values()))
        new.bytes = sum(list(self.bytes.values()))
        new.bytes_var = var(list(self.bytes.values()))
        new.start = self.start
        new.end = Phase.verify_end(new.start, self.end, new.time)
        new.reqs = self.reqs
        return new

    def collapse_shared(self, threshold, nproc, darshan):
        """
        Parameters
        ----------
        threshold : float
            how many processes can be out of this phase for
            it to still be considered -1 (all ranks).
            0 <= threshold <= 1
        nproc : int
            how many processes executed this job
        darshan : int
            the darshan version used for this trace (2 or 3)

        Returns
        -------
        Phase
            a phase that represents this whole collection
            together.
        """
        if len(self.ranks) == 0: #this is empty
            return Phase(darshan)
        elif len(self.ranks) == 1: #single phase
            return self.last
        else:  #multiple phases to be combined
            #we use self.last which is the last phase we
            #added to this collection, because it is
            #already filled with some fields we don't have
            #to change
            self.last.bytes = sum(list(self.bytes.values()))
            self.last.bytes_var = var(list(self.bytes.values()))
            if Phase.use_all_ranks(len(self.ranks), nproc, threshold):
                self.last.rank = -1
                self.last.subset_mode = nproc
                self.last.subset_avg = nproc
            else:
                self.last.rank = - len(self.ranks)
                self.last.subset_mode = len(self.ranks)
                self.last.subset_avg = len(self.ranks)
            self.last.files = "shared"
            self.last.time = max(list(self.time_dict.values()))
            self.last.time_var = var(list(self.time_dict.values()))
            self.last.start = self.start
            self.last.end = Phase.verify_end(self.start, self.end, self.last.time)
            self.last.reqs = self.reqs
            cons = value_or_zero(self.spatiality, "contiguous")
            seq = cons + value_or_zero(self.spatiality, "sequential")
            self.last.spatiality = self.last.decide_spatiality(cons, seq, cons, threshold)
            if (self.last.spatiality == "contiguous") and (self.last.api == "mpiio"):
                #this is a collective operation
                #that for some reason was reported
                #separately to each rank, in that case
                #the number of reqs must be divided by
                #the number of ranks involved because
                #otherwise we are counting each
                #collective operation multiple times
                self.last.reqs = int(float(self.last.reqs) / len(self.ranks))
                assert (self.last.reqs > 0) or (self.last.bytes <= 0)
            self.last.rank_list = list(self.ranks.keys())
            return self.last


